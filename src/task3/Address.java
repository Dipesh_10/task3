package task3;

public class Address {
	private String country, city, street;
	private int streetNum;
		
	public Address() {
		
	}
	
	public Address(String country, String city, String street, int streetNum) {
		this.country = country;
		this.city = city;
		this.street = street;
		this.streetNum = streetNum;
	}
	
	@Override
	public String toString() {
		return "Address [country=" + country + ", city=" + city + ", street=" + street + ", streetNum=" + streetNum
				+ "]";
	}
}
