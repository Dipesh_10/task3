package task3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MyTest {
	private Manager manager = new Manager();
	private Developer developer = new Developer();
	private Address address = new Address();

	@BeforeEach
	public void test() {
		manager = new Manager("manager", "manager", 2464, 123);
		developer = new Developer("developer1", "developer1", 144);
		address = new Address("address1", "address1", "address1", 12);
		manager.setAddress(address);
		developer.setAddress(address);
		manager.addSubordinates(developer);
	}

	@Test
	void testManagerSalary() {
		assertEquals(2587, manager.getSalary());
	}

	@Test
	void testDeveloperSalary() {
		assertEquals(144, developer.getSalary());
	}

	@Test
	void testAddSubordinate() {
		assertTrue(manager.getSubordinates().contains(developer));
	}

	@Test
	void testManagerToString() {
		// when
		String displayManager = "[salary=2587, name=manager, surname=manager] Address [country=address1, city=address1, street=address1, streetNum=12]\r\nSubordinate [salary=144, name=developer1, surname=developer1] Address [country=address1, city=address1, street=address1, streetNum=12]\r\n";

		// then
		assertEquals(displayManager, manager.toString());
	}

	@Test
	void testDeveloperToString() {
		// when
		String displayDeveloper = "[salary=144, name=developer1, surname=developer1] Address [country=address1, city=address1, street=address1, streetNum=12]";

		// then
		assertEquals(displayDeveloper, developer.toString());
	}
}
