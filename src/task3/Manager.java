package task3;

import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {
	private int bonus;
	private List<Developer> subordinates = new ArrayList<Developer>();

	public Manager() {
		
	}
	
	public Manager(String name, String surname, int salary, int bonus) {
		super(name, surname, salary);
		this.bonus = bonus;
	}

	public void addSubordinates(Developer developer) {
		subordinates.add(developer);
	}

	@Override 
	public int getSalary() {
		return super.getSalary() + bonus;
	}

	@Override 
	public String toString() {
		String subordinatesList = "";
			for (Developer developer : subordinates) {
				subordinatesList += "Subordinate " + (developer).toString()+"\r\n";
			}
		return super.toString() +"\r\n"+ subordinatesList;
	}

	public List<Developer> getSubordinates() {
		return subordinates;
	}


	
	
}

