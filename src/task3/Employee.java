package task3;

public abstract class Employee {
	private int salary;
	private String name, surname;
	private Address address;

	public Employee() {
		
	}
	
	public Employee(String name, String surname, int salary) {
		this.name = name;
		this.surname = surname;
		this.salary = salary;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getSalary() {
		return this.salary;
	}

	@Override
	public String toString() {
		return "[salary=" + getSalary() + ", name=" + name + ", surname=" + surname + "] " + address.toString();
	}


}

