package task3;

public class Main {
	public static void main(String[] args) {
		Employee developer1 = new Developer("developer1", "developer1", 144);
		Employee developer2 = new Developer("developer1", "developer1", 254);
		Address developer1_address = new Address("address1", "address1", "address1", 12);
		Address developer2_address = new Address("address2", "address2", "address2", 12);
		developer1.setAddress(developer1_address);
		developer2.setAddress(developer2_address);
		
		Employee manager = new Manager("manager", "manager", 2464, 123);
		Address manager_address = new Address("address3", "address3", "address3", 12);
		manager.setAddress(manager_address);
		((Manager) manager).addSubordinates((Developer) developer1);
		((Manager) manager).addSubordinates((Developer) developer2);
	
		System.out.println(developer1);
		System.out.println(manager);
	}
}

